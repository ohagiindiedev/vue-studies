import { http } from './config'

export default {
    listar: () => {
        return http.get('tarefas')
    },
    adicionar: (tarefa) => {
        return http.post('tarefas', tarefa)
    },
    remover: (id) => {
        return http.delete('tarefas/'+id)
    }
}